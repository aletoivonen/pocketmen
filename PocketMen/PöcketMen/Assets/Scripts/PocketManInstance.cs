﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PocketManInstance
{
    [SerializeField]
    public PocketManInstance(PocketMan pConfig)
    {
        config = pConfig;
        pp = config.maxPp;
        level = 1;
        hp = config.maxHp;
        statuses = new Dictionary<StatusEffect, int>();
    }

    [SerializeField]
    public PocketMan config;
    public int pp;
    public int level;
    public int hp;
    public Dictionary<StatusEffect,int> statuses;
}
