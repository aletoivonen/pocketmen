﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum StatusEffect
{
    NONE = 0,
    SLEEP,
    BURN,
    PARALYSIS
}

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Move", order = 2)]
[System.Serializable]
public class Move : ScriptableObject
{
    public string moveName;
    public int power;
    public int ppCost;
    public StatusEffect effect;
    public float effectChance;
    public string animation;
    public bool animateFriendly = false;
    public string taunt = "";
}
