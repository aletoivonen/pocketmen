﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum PocketManType
{
    STRONG = 0,
    SMART,
    NORMAL,
    BUSINESS
}

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PocketMan", order = 1)]
[System.Serializable]
public class PocketMan : ScriptableObject
{
    public string id;
    public string visibleName;
    public PocketManType type;
    public int maxHp;
    public int maxPp;
    public int attack;
    public int defence;
    public string greeting = "";

    public Sprite friendlySprite; // sprite when friendly
    public Sprite opponentSprite; // sprite when opponent

    public Move[] moves;

    public string TypeString()
    {
        switch (type)
        {
            case PocketManType.STRONG:
                {
                    return "Strong";
                }
            case PocketManType.SMART:
                {
                    return "Smart";
                }
            case PocketManType.NORMAL:
                {
                    return "Normal";
                }
            case PocketManType.BUSINESS:
                {
                    return "Business";
                }
            default:
                {
                    return "UNKNOWN";
                }
        }
    }

}

[System.Serializable]
public struct SpawnProbability
{
    public PocketMan config;
    public float spawnChance;
}
