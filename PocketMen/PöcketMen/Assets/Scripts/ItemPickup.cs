﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    [SerializeField]
    private CollectableItem config;

    public void PickUp()
    {
        AudioController.Instance.CollectSound();
        PlayerController.Instance.AddItem(new ItemInstance(config));
        Destroy(gameObject);
    }
}
