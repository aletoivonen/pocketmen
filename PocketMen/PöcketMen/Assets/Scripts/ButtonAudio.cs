﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour
{
    public bool Major;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if (Major)
            {
                AudioController.Instance.SelectSound();
            }
            else
            {
                AudioController.Instance.ButtonSound();
            }
        });
    }
}
