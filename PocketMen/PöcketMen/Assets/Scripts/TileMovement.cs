﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMovement : MonoBehaviour
{
    [SerializeField]
    private LayerMask solidLayer;
    [SerializeField]
    private LayerMask triggerLayer;
    [SerializeField]
    private float movingSpeed = 1.0f;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private bool isMoving = false;

    public bool canMove = true;

    [SerializeField]
    private Vector3 offset = Vector3.zero;

    void Update()
    {
        if (!isMoving && canMove)
        {
            Vector2 input = Vector2.zero;
            input.x = Input.GetAxisRaw("Horizontal");
            input.y = Input.GetAxisRaw("Vertical");

            if (input != Vector2.zero)
            {
                if (input.x != 0.0f) input.y = 0.0f;

                animator.SetFloat("moveX", input.x);
                animator.SetFloat("moveY", input.y);

                Vector3 targetPos = transform.position;
                targetPos.x += input.x;
                targetPos.y += input.y;

                if (IsTileWalkable(targetPos))
                {
                    StartCoroutine(MoveToPosition(targetPos));
                }
            }
        }

        animator.SetBool("moving", isMoving);
    }

    IEnumerator MoveToPosition(Vector3 targetPos)
    {
        isMoving = true;
        while ((targetPos - transform.position).sqrMagnitude > Mathf.Epsilon)
        {
            bool sprint = Input.GetAxisRaw("Sprint") == 1;
            transform.position = Vector3.MoveTowards(transform.position, targetPos, movingSpeed * (sprint ? 2.0f : 1.0f) * Time.deltaTime);
            yield return null;
        }
        transform.position = targetPos;
        isMoving = false;

        CheckTriggers(targetPos);
    }

    private bool IsTileWalkable(Vector3 tile)
    {
        return Physics2D.OverlapCircle(tile, 0.15f, solidLayer) == null;
    }

    private bool CheckTriggers(Vector3 tile)
    {
        Collider2D collider = Physics2D.OverlapCircle(tile, 0.15f, triggerLayer);
        if (collider != null && collider.CompareTag("Portal"))
        {
            Debug.Log("entering portal " + collider.name);
            Teleport(collider.GetComponent<Portal>().targetLocation.position);
            return true;
        }
        if (collider != null && collider.CompareTag("Encounter"))
        {
            Debug.Log("encounter " + collider.name);
            collider.GetComponent<EncounterTrigger>().CheckEncounter();
            return true;
        }
        if (collider != null && collider.CompareTag("Spa") && PlayerController.Instance.ShouldUseSpa())
        {
            canMove = false;
            TransitionController.Instance.SwipeTransition(() =>
            {
                PlayerController.Instance.UseSpa();
            },
            () =>
            {
                DialogueController.Instance.ShowDialogue("All your PöcketMen have been healed!", () =>
                {
                    canMove = true;
                });
            });
            return true;
        }
        if (collider != null && collider.CompareTag("Pickup"))
        {
            collider.GetComponent<ItemPickup>().PickUp();
            return true;
        }
        if (collider != null && collider.CompareTag("Dialogue"))
        {
            canMove = !collider.GetComponent<DialogueTrigger>().ShowDialogue(() =>
            {
                canMove = true;
            });
            return !canMove;
        }
        return false;
    }

    private void Teleport(Vector3 tile)
    {
        canMove = false;
        TransitionController.Instance.FadeTransition(() =>
        {
            transform.position = tile + offset;
        }, () =>
        {
            canMove = true;
        });
    }
}
