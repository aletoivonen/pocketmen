﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class InvasionAlert : MonoBehaviour
{
    public static InvasionAlert Instance;

    [SerializeField]
    private GameObject parent;
    [SerializeField]
    private Image image;

    public bool beingInvaded = false;
    private void Awake()
    {
        Instance = this;
    }
    public void GetInvaded(Action OnAlertDone)
    {
        beingInvaded = true;
        StartCoroutine(Fade(OnAlertDone));
    }

    IEnumerator Fade(Action OnAlertDone)
    {
        parent.SetActive(true);
        for (int i = 0; i < 3; i++)
        {
            float t = 0.0f;
            while (t < 1.0f)
            {
                t += Time.deltaTime / 1.5f;
                Color c = image.color;
                c.a = Mathf.Sin(t * Mathf.PI);
                image.color = c;
                yield return null;
            }
        }
        parent.SetActive(false);
        OnAlertDone();
    }
}
