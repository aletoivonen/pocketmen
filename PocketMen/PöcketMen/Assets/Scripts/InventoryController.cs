﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[System.Serializable]
public class ItemInstance
{
    public CollectableItem config;

    public ItemInstance(CollectableItem pConfig)
    {
        config = pConfig;
    }
}

public class InventoryController : MonoBehaviour
{
    public static InventoryController Instance;
    public static Action OnUseItem;
    public static Action<bool> OnClose;

    [SerializeField]
    private GameObject parent;

    [SerializeField]
    private GameObject fade;

    [SerializeField]
    private GameObject inventoryItemPrefab;

    [SerializeField]
    private GameObject buttonPrefab;

    [SerializeField]
    private RectTransform inventoryGrid;

    [SerializeField]
    private GameObject actionMenu;

    [SerializeField]
    private GameObject pocketManMenu;
    [SerializeField]
    private RectTransform pocketManList;

    private List<InventoryItem> spawnedItems = new List<InventoryItem>();

    private bool usedItem = false;

    private void Awake()
    {
        Instance = this;
    }

    public void OpenInventory()
    {
        usedItem = false;
        parent.SetActive(true);
        SetupGrid();
    }

    public void CloseInventory()
    {
        parent.SetActive(false);
        if (OnClose != null)
            OnClose(usedItem);
    }

    private ItemInstance pendingItemInstance;
    public void TryUseItem(ItemInstance itemInstance)
    {
        pendingItemInstance = itemInstance;
        actionMenu.SetActive(true);
        fade.SetActive(true);
    }

    public void ConfirmUseItem()
    {
        if (pendingItemInstance.config.targetsPocketMan)
        {
            actionMenu.SetActive(false);
            PopulatePokemanList();
            pocketManList.gameObject.SetActive(true);
        }
        else
        {
            ExecuteUseItem();
            actionMenu.SetActive(false);
            fade.SetActive(false);
            RefreshItems();
        }
    }

    public void UseItemOnPocketMan(PocketManInstance pocketManInstance)
    {
        ExecuteUseItem(pocketManInstance);
        pocketManMenu.SetActive(false);
        fade.SetActive(false);
        RefreshItems();
    }

    private void ExecuteUseItem(PocketManInstance pocketManInstance = null)
    {
        PlayerController.Instance.UseItem(pendingItemInstance, pocketManInstance);
        pendingItemInstance = null;
        usedItem = true;
        if (OnUseItem != null)
            OnUseItem();
    }

    public void CancelUseItem()
    {
        pocketManMenu.SetActive(false);
        actionMenu.SetActive(false);
        fade.SetActive(false);
    }

    private void PopulatePokemanList()
    {
        List<GameObject> destroyList = new List<GameObject>();
        foreach (Transform child in pocketManList)
        {
            destroyList.Add(child.gameObject);
        }
        foreach (GameObject g in destroyList)
        {
            Destroy(g);
        }

        GameObject cancelButton = Instantiate(buttonPrefab);
        cancelButton.transform.SetParent(pocketManList, false);
        cancelButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            CancelUseItem();
        });
        cancelButton.GetComponentInChildren<Text>().text = "CANCEL";

        foreach (PocketManInstance instance in PlayerController.Instance.pocketManInstances)
        {
            GameObject button = Instantiate(buttonPrefab);
            button.transform.SetParent(pocketManList, false);
            button.GetComponent<Button>().onClick.AddListener(() =>
            {
                UseItemOnPocketMan(instance);
            });
            button.GetComponentInChildren<Text>().text = instance.config.visibleName.ToUpper() + " LV" + instance.level;
        }
    }

    private void SetupGrid()
    {
        foreach (ItemInstance itemInstance in PlayerController.Instance.itemInstances)
        {
            InventoryItem spawnedItem = spawnedItems.Find(item => item.itemId == itemInstance.config.id);
            bool existed = spawnedItem != null;

            if (!existed)
            {
                GameObject newGo = Instantiate(inventoryItemPrefab);
                newGo.transform.SetParent(inventoryGrid, false);
                spawnedItem = newGo.GetComponent<InventoryItem>();
                spawnedItem.SetupItem(itemInstance);
                spawnedItems.Add(spawnedItem);
            }
            else
            {
                spawnedItem.Refresh();
            }
        }
    }

    public void RefreshItems()
    {
        foreach (Transform child in inventoryGrid)
        {
            child.GetComponent<InventoryItem>().Refresh();
        }
    }

}
