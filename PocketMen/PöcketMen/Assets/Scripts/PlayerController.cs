﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public List<PocketManInstance> pocketManInstances = new List<PocketManInstance>();
    public List<PocketManInstance> pocketManDump = new List<PocketManInstance>();

    public List<ItemInstance> itemInstances = new List<ItemInstance>();

    public PocketManInstance selectedPocketMan;

    public string playerName = "Navy Seal";

    [SerializeField]
    private PocketMan defaultPocketMan;
    [SerializeField]
    private PocketMan defaultPocketMan2;
    [SerializeField]
    private PocketMan defaultPocketMan3;
    [SerializeField]
    private PocketMan defaultPocketMan4;
    [SerializeField]
    private PocketMan defaultPocketMan5;
    [SerializeField]
    private CollectableItem defaultItem1;
    [SerializeField]
    private CollectableItem defaultItem2;

    private TileMovement movementScript;

    private void Awake()
    {
        movementScript = GetComponent<TileMovement>();
        Instance = this;
    }

    private void Start()
    {
        AddPocketMan(new PocketManInstance(defaultPocketMan));
        AddPocketMan(new PocketManInstance(defaultPocketMan2));
        AddPocketMan(new PocketManInstance(defaultPocketMan3));
        AddPocketMan(new PocketManInstance(defaultPocketMan4));
        AddPocketMan(new PocketManInstance(defaultPocketMan5));
        AddItem(new ItemInstance(defaultItem1));
        AddItem(new ItemInstance(defaultItem2));
        AddItem(new ItemInstance(defaultItem1));
        AddItem(new ItemInstance(defaultItem2));
        AudioController.Instance.ChooseMusic(Music.GAME);
    }

    public void ToggleMovement(bool canMove)
    {
        movementScript.canMove = canMove;
    }

    public void AddPocketMan(PocketManInstance instance)
    {
        pocketManInstances.Add(instance);
        if (selectedPocketMan?.config == null)
        {
            selectedPocketMan = instance;
        }
    }

    public void AddItem(ItemInstance instance)
    {
        itemInstances.Add(instance);
    }
    public void UseItem(ItemInstance instance, PocketManInstance pocketManInstance = null)
    {
        ItemInstance matchingInstance = itemInstances.Find(item => item.config.id == instance.config.id);
        if (matchingInstance != null)
        {
            itemInstances.Remove(matchingInstance);
            ProcessItemUse(matchingInstance.config.id, pocketManInstance);
            InventoryController.Instance.RefreshItems();
        }
    }

    private void ProcessItemUse(string itemId, PocketManInstance pocketManInstance = null)
    {
        switch (itemId)
        {
            case "vihreakuula":
                {
                    pocketManInstance.hp = Mathf.Clamp(pocketManInstance.hp + 50, 0, pocketManInstance.config.maxHp);
                    break;
                }
            case "lonkero":
                {
                    pocketManInstance.pp = Mathf.Clamp(pocketManInstance.pp + 10, 0, pocketManInstance.config.maxPp);
                    break;
                }
        }
    }

    public bool AnyAlive()
    {
        return pocketManInstances.Find(instance => instance.hp > 0) != null;
    }
    public bool MovesAvailable()
    {
        return selectedPocketMan.config.moves.Any(move => move.ppCost <= selectedPocketMan.pp);
    }

    public void SelectPocketMan(PocketManInstance instance)
    {
        selectedPocketMan = instance;
    }

    public void UseSpa()
    {
        foreach (PocketManInstance instance in pocketManInstances)
        {
            instance.hp = instance.config.maxHp;
            instance.pp = instance.config.maxPp;
        }
    }

    public bool ShouldUseSpa()
    {
        bool should = false;
        foreach (PocketManInstance instance in pocketManInstances)
        {
            if (instance.hp < instance.config.maxHp || instance.pp < instance.config.maxPp)
            {
                should = true;
                break;
            }
        }
        return should;
    }
}
