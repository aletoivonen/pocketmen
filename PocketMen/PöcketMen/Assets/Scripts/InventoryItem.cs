﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text textField;
    [SerializeField]
    private Text amountField;

    private ItemInstance itemInstance;

    public string itemId { get { return itemInstance.config.id; } }

    public void SetupItem(ItemInstance item)
    {
        itemInstance = item;
        image.sprite = item.config.sprite;
        textField.text = item.config.visibleName;
        Refresh();
    }

    public void Refresh()
    {
        var matches = PlayerController.Instance.itemInstances.FindAll(item => item.config.id == itemInstance.config.id);
        int amount = matches.Count;
        if (amount <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            amountField.text = amount > 1 ? amount.ToString() : "";
        }
    }

    public void UseItem()
    {
        InventoryController.Instance.TryUseItem(itemInstance);
    }
}
