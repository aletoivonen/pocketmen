﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[System.Serializable]
public enum ActionChoice
{
    PENDING = 0,
    FIGHT,
    BAG,
    SWITCH,
    RUN
}

public enum FightPhase
{
    NARRATION = 0,
    ACTION_SELECT,
    MOVE_SELECT
}

public class FightController : MonoBehaviour
{
    public static FightController Instance;

    [SerializeField]
    private GameObject canvas;

    [SerializeField]
    private GameObject textButtonPrefab;

    [SerializeField]
    private Image playerSprite;
    [SerializeField]
    private Image opponentSprite;
    [SerializeField]
    private Image playerPokeManSprite;
    [SerializeField]
    private Image opponentPokeManSprite;

    [SerializeField]
    private GameObject narrationView;
    [SerializeField]
    private Text narrationField;

    [SerializeField]
    private GameObject actionView;
    [SerializeField]
    private RectTransform actionGridContainer;

    [SerializeField]
    private GameObject moveView;
    [SerializeField]
    private RectTransform moveGridContainer;

    [SerializeField]
    private RectTransform boxPlayer;
    [SerializeField]
    private RectTransform boxOpponent;

    [SerializeField]
    private FightStatDisplay playerStatdisplay;
    [SerializeField]
    private FightStatDisplay opponentStatdisplay;

    [SerializeField]
    private GameObject statScreen;
    [SerializeField]
    private Text ppCounterField;
    [SerializeField]
    private Text typeField;

    [SerializeField]
    private Text playerGreeting;
    [SerializeField]
    private Text opponentGreeting;

    [SerializeField]
    private Animator opponentEffects;
    [SerializeField]
    private Animator playerEffects;

    private PocketManInstance playerPokeManInstance;
    private PocketManInstance opponentPokeManInstance;

    // TURN STUFF
    ActionChoice currentActionChoice = ActionChoice.PENDING;
    Move currentMoveChoice = null;

    private NpcConfig currentOpponent;
    private int npcLastPocketManIndex = 0;

    private bool isPlayersTurn = true;
    private bool usedItem = false;
    private bool switchedPocketMan = false;
    private bool cantFight = false;
    private bool waitingForReplacement = false;

    [SerializeField]
    private NpcConfig testNpc;

    private void Awake()
    {
        Instance = this;
        //StartFight(null, null, new PocketManInstance(testPlayer), new PocketManInstance(testOpponent));
    }

    public void StartFight(NpcConfig opponent)
    {
        currentOpponent = opponent;
        opponentSprite.sprite = opponent.sprite;
        npcLastPocketManIndex = 0;
        opponentPokeManInstance = new PocketManInstance(currentOpponent.pocketMen[npcLastPocketManIndex]);
        InitFight();
    }

    public void StartFight(PocketManInstance opponentPokeman)
    {
        currentOpponent = null;
        opponentPokeManInstance = opponentPokeman;
        InitFight();
    }

    void InitFight()
    {
        AudioController.Instance.ChooseMusic(Music.BATTLE);
        playerPokeManSprite.sprite = PlayerController.Instance.selectedPocketMan.config.friendlySprite;
        opponentPokeManSprite.sprite = opponentPokeManInstance.config.opponentSprite;

        PlayerController.Instance.ToggleMovement(false);

        TransitionController.Instance.SwipeTransition(() =>
        {
            canvas.SetActive(true);

            playerPokeManInstance = PlayerController.Instance.selectedPocketMan;

            StartCoroutine(AnimateFightStart());

        }, null);
    }

    IEnumerator AnimateNarration(string text, float waitAfter = 2.0f)
    {
        yield return DialogueController.Instance.AnimateText(text, narrationField, waitAfter);
    }

    IEnumerator AnimateFightStart()
    {
        playerStatdisplay.gameObject.SetActive(false);
        opponentStatdisplay.gameObject.SetActive(false);
        bool opponent = currentOpponent != null;
        RefreshUI();
        playerSprite.rectTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
        boxPlayer.anchoredPosition = new Vector2(-307.0f, 0.0f);
        opponentSprite.rectTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
        boxOpponent.anchoredPosition = new Vector2(307.0f, 0.0f);
        playerPokeManSprite.enabled = false;
        opponentPokeManSprite.enabled = !opponent;
        opponentSprite.enabled = opponent;
        if (opponent)
        {
            yield return AnimateNarration(currentOpponent.visibleName + " has challenged " + PlayerController.Instance.playerName);
            yield return AnimateNarration("\"" + currentOpponent.greetingMessage + "\"");

            /*narrationField.text = currentOpponent.visibleName + " has challenged " + PlayerController.Instance.playerName;
            yield return new WaitForSeconds(2.0f);
            narrationField.text = "\"" + currentOpponent.greetingMessage + "\"";
            yield return new WaitForSeconds(2.0f);*/
        }
        else
        {
            yield return AnimateNarration("a wild " + opponentPokeManInstance.config.visibleName + " appears!");
        }

        Vector2 playerPos = playerSprite.rectTransform.anchoredPosition;
        Vector2 playerBallPos = boxPlayer.anchoredPosition;
        Vector2 opponentPos = opponentSprite.rectTransform.anchoredPosition;
        Vector2 opponentBallPos = boxOpponent.anchoredPosition;
        float t = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime / 0.5f;
            playerPos.x = -t * 350.0f;
            playerSprite.rectTransform.anchoredPosition = playerPos;
            if (opponent)
            {
                opponentPos.x = t * 350.0f;
                opponentSprite.rectTransform.anchoredPosition = opponentPos;
            }
            yield return null;
        }

        boxPlayer.GetComponent<Animator>().SetBool("throw_red", true);
        if (opponent)
        {
            boxOpponent.GetComponent<Animator>().SetBool("throw_red", true);
        }
        AudioController.Instance.WooshSound();

        t = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime / 1.25f;
            playerBallPos.x = (1 - t) * -307.0f;
            boxPlayer.anchoredPosition = playerBallPos;
            if (opponent)
            {
                opponentBallPos.x = (1 - t) * 307.0f;
                boxOpponent.anchoredPosition = opponentBallPos;
            }
            yield return null;
        }
        AudioController.Instance.SummonSound();

        playerBallPos.x = -307.0f;
        boxPlayer.anchoredPosition = playerBallPos;
        if (opponent)
        {
            opponentBallPos.x = 307.0f;
            boxOpponent.anchoredPosition = opponentBallPos;
        }
        playerPokeManSprite.enabled = true;
        opponentPokeManSprite.enabled = true;
        playerStatdisplay.gameObject.SetActive(true);
        opponentStatdisplay.gameObject.SetActive(true);

        playerGreeting.text = playerPokeManInstance.config.greeting;
        opponentGreeting.text = opponentPokeManInstance.config.greeting;

        yield return new WaitForSeconds(3.0f);

        playerGreeting.text = "";
        opponentGreeting.text = "";

        isPlayersTurn = true;
        StartCoroutine(ProcessTurn());
    }

    public void ChooseAction(int action)
    {
        ActionChoice choice = (ActionChoice)action;
        if (choice == ActionChoice.FIGHT)
        {
            currentActionChoice = ActionChoice.FIGHT;
        }
        if (choice == ActionChoice.BAG)
        {
            currentActionChoice = choice;
            InventoryController.Instance.OpenInventory();
            InventoryController.OnUseItem += ForceCloseInventory;
            InventoryController.OnClose += OnInventoryClosed;
        }
        if (choice == ActionChoice.SWITCH)
        {
            currentActionChoice = choice;
            PocketManSelector.Instance.OpenSelector();
            PocketManSelector.OnClose += OnSelectorClosed;
        }
        if (choice == ActionChoice.RUN)
        {
            EndFight(opponentPokeManInstance, playerPokeManInstance, false, false);
        }
    }

    void ForceCloseInventory()
    {
        InventoryController.OnUseItem -= ForceCloseInventory;
        InventoryController.Instance.CloseInventory();
    }

    void OnInventoryClosed(bool used)
    {
        InventoryController.OnClose -= OnInventoryClosed;
        if (used)
        {
            currentActionChoice = ActionChoice.FIGHT;
            usedItem = true;
        }
    }

    void OnSelectorClosed(bool switched)
    {
        PocketManSelector.OnClose -= OnSelectorClosed;
        playerPokeManInstance = PlayerController.Instance.selectedPocketMan;
        playerPokeManSprite.sprite = playerPokeManInstance.config.friendlySprite;
        RefreshUI();
        if (switched)
        {
            currentActionChoice = ActionChoice.FIGHT;
            switchedPocketMan = true;
        }
    }

    void SetFightPhase(FightPhase phase)
    {
        narrationView.SetActive(phase == FightPhase.NARRATION || phase == FightPhase.ACTION_SELECT);
        actionView.SetActive(phase == FightPhase.ACTION_SELECT);
        moveView.SetActive(phase == FightPhase.MOVE_SELECT);
        statScreen.SetActive(phase == FightPhase.MOVE_SELECT);
    }

    void PickMove(Move move)
    {
        if (move.ppCost > playerPokeManInstance.pp)
            return;
        currentMoveChoice = move;
        playerPokeManInstance.pp -= move.ppCost;
        RefreshUI();
    }

    IEnumerator ProcessTurn()
    {
        switchedPocketMan = false;
        cantFight = false;
        usedItem = false;
        RefreshUI();
        PocketManInstance pokeMan = isPlayersTurn ? playerPokeManInstance : opponentPokeManInstance;
        PocketManInstance otherPokeMan = !isPlayersTurn ? playerPokeManInstance : opponentPokeManInstance;

        bool skip = false;

        if (pokeMan.statuses.ContainsKey(StatusEffect.SLEEP) && pokeMan.statuses[StatusEffect.SLEEP]-- > 0)
        {
            yield return AnimateNarration(pokeMan.config.visibleName + " is " + GetStatusActiveNarration(StatusEffect.SLEEP) + "!", 1.5f);
            cantFight = true;
            PlayMoveAnimation(isPlayersTurn ? playerEffects : opponentEffects, "sleep");
        }
        else
        {
            PlayMoveAnimation(isPlayersTurn ? playerEffects : opponentEffects, "idle");
        }

        if (pokeMan.statuses.ContainsKey(StatusEffect.PARALYSIS) && pokeMan.statuses[StatusEffect.PARALYSIS]-- > 0)
        {
            yield return AnimateNarration(pokeMan.config.visibleName + " is " + GetStatusActiveNarration(StatusEffect.PARALYSIS) + "!", 1.5f);
            skip = true;
        }

        if (pokeMan.statuses.ContainsKey(StatusEffect.BURN) && pokeMan.statuses[StatusEffect.BURN]-- > 0)
        {
            DamagePocketMan(pokeMan, 10);
            RefreshUI();
            yield return AnimateNarration(pokeMan.config.visibleName + " is " + GetStatusActiveNarration(StatusEffect.BURN) + "!", 1.5f);
        }

        if (pokeMan.hp > 0 && !skip)
        {
            SetFightPhase(isPlayersTurn ? FightPhase.ACTION_SELECT : FightPhase.NARRATION);
            if (isPlayersTurn || !cantFight)
            {
                yield return AnimateNarration(pokeMan.config.visibleName + "'s turn", isPlayersTurn ? 1.0f : 3.0f);
            }

            currentActionChoice = isPlayersTurn ? ActionChoice.PENDING : ActionChoice.FIGHT;
            while (currentActionChoice != ActionChoice.FIGHT)
            {
                yield return null;
            }

            if (!usedItem && !switchedPocketMan && !cantFight)
            {
                PopulateFightButtons(pokeMan.config);
                SetFightPhase(FightPhase.MOVE_SELECT);

                if (isPlayersTurn)
                {
                    currentMoveChoice = null;
                    while (currentMoveChoice == null)
                    {
                        yield return null;
                    }
                }
                else
                {
                    Move[] validMoves = pokeMan.config.moves.Where(move => move.ppCost <= pokeMan.pp).ToArray();
                    if (validMoves == null || validMoves.Length == 0)
                    {
                        EndFight(playerPokeManInstance, opponentPokeManInstance, true, false);
                    }
                    currentMoveChoice = validMoves[Random.Range(0, validMoves.Length)];
                }

                if (!PlayerController.Instance.MovesAvailable() || cantFight)
                {
                    yield return AnimateNarration(pokeMan.config.visibleName + " can't attack!", 1.5f);

                }
                else
                {
                    StatusEffect statusApplied = StatusEffect.NONE;
                    int dmg = ExecuteMove(currentMoveChoice, pokeMan, otherPokeMan, ref statusApplied);
                    if (!string.IsNullOrEmpty(currentMoveChoice.taunt))
                    {
                        (isPlayersTurn ? playerGreeting : opponentGreeting).text = currentMoveChoice.taunt;
                    }

                    RefreshUI();
                    SetFightPhase(FightPhase.NARRATION);
                    yield return AnimateNarration(pokeMan.config.visibleName + " used " + currentMoveChoice.moveName.ToUpper() + "!", 1.5f);

                    (isPlayersTurn ? playerGreeting : opponentGreeting).text = "";

                    if (statusApplied != StatusEffect.NONE)
                    {
                        yield return AnimateNarration(otherPokeMan.config.visibleName + " got " + GetStatusAppliedNarration(statusApplied) + "!", 1.5f);
                    }
                }
            }
            else if (usedItem)
            {
                SetFightPhase(FightPhase.NARRATION);
                RefreshUI();
                yield return AnimateNarration(pokeMan.config.visibleName + " used an item!");
            }
            else if (switchedPocketMan)
            {
                SetFightPhase(FightPhase.NARRATION);
                RefreshUI();
                pokeMan = playerPokeManInstance;
                yield return AnimateNarration((isPlayersTurn ? PlayerController.Instance.playerName : currentOpponent.visibleName) + " switched active PöcketMan!");
            }

        }

        if (pokeMan.hp <= 0 || otherPokeMan.hp <= 0)
        {
            SetFightPhase(FightPhase.NARRATION);
            yield return AnimateNarration(otherPokeMan.config.visibleName + " fainted!", 3.0f);

            if (!isPlayersTurn && PlayerController.Instance.AnyAlive())
            {
                narrationField.text = "";
                waitingForReplacement = true;
                PocketManSelector.Instance.OpenSelector();
                PocketManSelector.OnClose += SelectReplacementPocketMan;

                while (waitingForReplacement)
                    yield return null;

                playerPokeManInstance = PlayerController.Instance.selectedPocketMan;
                playerPokeManSprite.sprite = playerPokeManInstance.config.friendlySprite;
                RefreshUI();

                playerGreeting.text = playerPokeManInstance.config.greeting;
                yield return new WaitForSeconds(1.5f);
                playerGreeting.text = "";
            }
            else if (isPlayersTurn)
            {
                npcLastPocketManIndex++;
                if (npcLastPocketManIndex < currentOpponent?.pocketMen.Length)
                {
                    opponentPokeManInstance = new PocketManInstance(currentOpponent.pocketMen[npcLastPocketManIndex]);
                    opponentPokeManSprite.sprite = opponentPokeManInstance.config.opponentSprite;
                    RefreshUI();
                    opponentGreeting.text = opponentPokeManInstance.config.greeting;
                    yield return new WaitForSeconds(1.5f);
                    opponentGreeting.text = "";
                    yield return AnimateNarration(currentOpponent.visibleName + " chose " + opponentPokeManInstance.config.visibleName + "!");
                }
                else
                {
                    EndFight(pokeMan, otherPokeMan, isPlayersTurn, currentOpponent == null);
                    yield break;
                }
            }
            else
            {
                EndFight(pokeMan, otherPokeMan, isPlayersTurn, false);
                yield break;
            }
        }

        isPlayersTurn = !isPlayersTurn;
        StartCoroutine(ProcessTurn());
    }

    private string GetStatusAppliedNarration(StatusEffect statusApplied)
    {
        switch (statusApplied)
        {
            case StatusEffect.BURN:
                {
                    return "BURNED";
                }
            case StatusEffect.PARALYSIS:
                {
                    return "PARALYZED";
                }
            default:
            case StatusEffect.SLEEP:
                {
                    return "SLEEPY";
                }
        }
    }

    private string GetStatusActiveNarration(StatusEffect statusApplied)
    {
        switch (statusApplied)
        {
            case StatusEffect.BURN:
                {
                    return "BURNING";
                }
            case StatusEffect.PARALYSIS:
                {
                    return "PARALYZED";
                }
            default:
            case StatusEffect.SLEEP:
                {
                    return "SLEEPING";
                }
        }
    }

    void SelectReplacementPocketMan(bool switched)
    {
        PocketManSelector.OnClose -= SelectReplacementPocketMan;
        if (switched)
        {
            waitingForReplacement = false;
        }
    }

    void PopulateFightButtons(PocketMan config)
    {
        List<GameObject> destroyList = new List<GameObject>();
        foreach (Transform child in moveGridContainer.transform)
        {
            destroyList.Add(child.gameObject);
        }
        foreach (GameObject g in destroyList)
        {
            Destroy(g);
        }
        foreach (Move move in config.moves)
        {
            GameObject button = Instantiate(textButtonPrefab);
            button.transform.SetParent(moveGridContainer, false);
            button.GetComponent<Button>().onClick.AddListener(() =>
           {
               PickMove(move);
           });
            button.GetComponentInChildren<Text>().text = move.moveName;
        }

    }

    public void EndFight(PocketManInstance winner, PocketManInstance loser, bool playerWin, bool catchOther)
    {
        AudioController.Instance.StopMusic();
        AudioController.Instance.WinSound();
        TransitionController.Instance.SwipeTransition(() =>
        {
            canvas.SetActive(false);
            AudioController.Instance.ChooseMusic(Music.GAME);
        }, () =>
        {
            PlayerController.Instance.ToggleMovement(true);
            if (playerWin && catchOther)
            {
                loser.hp = Mathf.RoundToInt((float)loser.config.maxHp / 2);
                PlayerController.Instance.AddPocketMan(loser);
            }
        });
    }

    void DamagePocketMan(PocketManInstance target, int damage)
    {
        AudioController.Instance.PlayRandomPunch();
        target.hp = Mathf.Clamp(target.hp - damage, 0, 10000);
    }

    void PlayMoveAnimation(Animator animator, string effect)
    {
        animator.SetTrigger(effect);
    }

    int ExecuteMove(Move move, PocketManInstance source, PocketManInstance target, ref StatusEffect statusApplied)
    {
        DamagePocketMan(target, move.power);
        if (move.effect != StatusEffect.NONE && Random.Range(0.0f, 1.0f) < move.effectChance)
        {
            AudioController.Instance.StatusEffectSound();
            if (target.statuses.ContainsKey(move.effect))
            {
                target.statuses[move.effect]++;
            }
            else
            {
                target.statuses[move.effect] = 1;
            }
            statusApplied = move.effect;
            if (!string.IsNullOrEmpty(move.animation))
            {
                PlayMoveAnimation((move.animateFriendly ? !isPlayersTurn : isPlayersTurn) ? opponentEffects : playerEffects, move.animation);
            }
        }
        return move.power;
    }

    void RefreshUI()
    {
        playerStatdisplay.Refresh(playerPokeManInstance);
        opponentStatdisplay.Refresh(opponentPokeManInstance);
        typeField.text = "TYPE/" + playerPokeManInstance.config.TypeString().ToUpper();
        ppCounterField.text = playerPokeManInstance.pp + "/" + playerPokeManInstance.config.maxPp;
    }

}
