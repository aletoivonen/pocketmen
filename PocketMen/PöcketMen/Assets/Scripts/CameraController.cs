﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 offset;

    private void Start()
    {
        offset = transform.position - PlayerController.Instance.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = PlayerController.Instance.transform.position + offset;
    }
}
