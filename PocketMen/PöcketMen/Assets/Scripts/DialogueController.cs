﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DialogueController : MonoBehaviour
{
    public static DialogueController Instance;

    [SerializeField]
    private GameObject parent;
    [SerializeField]
    private Text dialogueField;
    private void Awake()
    {
        Instance = this;
    }

    public void ShowDialogue(string[] dialogue, Action OnComplete = null)
    {
        StartCoroutine(DialogueIEnumerator(dialogue, OnComplete));
    }

    public void ShowDialogue(string dialogue, Action OnComplete = null)
    {
        StartCoroutine(DialogueIEnumerator(new string[1] { dialogue }, OnComplete));
    }

    IEnumerator DialogueIEnumerator(string[] dialogue, Action OnComplete = null)
    {
        parent.SetActive(true);
        for (int i = 0; i < dialogue.Length; i++)
        {
            yield return AnimateText(dialogue[i], dialogueField, 1.5f);
        }
        parent.SetActive(false);
        if (OnComplete != null)
            OnComplete();
    }

    public IEnumerator AnimateText(string content, Text textField, float pauseAfter)
    {
        textField.text = "";
        yield return new WaitForSeconds(0.5f);
        int letters = 1;
        int totalLetters = content.Length;
        while (letters < totalLetters)
        {
            letters++;
            textField.text = content.Substring(0, letters);
            yield return null;
        }
        yield return new WaitForSeconds(pauseAfter);
    }
}
