﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TransitionController : MonoBehaviour
{
    public static TransitionController Instance;

    [SerializeField]
    private Image image;
    [SerializeField]
    private float fadeSpeed = 0.3f;
    [SerializeField]
    private float swipeSpeed = 0.3f;
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        //StartCoroutine(TestRun());
    }

    /* IEnumerator TestRun()
     {
         //yield return Fade(null, null);
         //yield return Swipe(null, null);
     }*/

    public void FadeTransition(Action OnHidden, Action OnVisible)
    {
        StartCoroutine(Fade(OnHidden, OnVisible));
    }

    public void SwipeTransition(Action OnHidden, Action OnVisible)
    {
        StartCoroutine(Swipe(OnHidden, OnVisible));
    }

    Color c;
    IEnumerator Fade(Action OnHidden, Action OnVisible)
    {
        image.gameObject.SetActive(true);
        float t = 0.0f;
        c = image.color;
        c.a = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime / fadeSpeed;
            c.a = t;
            image.color = c;
            yield return null;
        }
        c.a = 1.0f;
        image.color = c;

        if (OnHidden != null)
            OnHidden();

        yield return new WaitForSeconds(0.4f);

        while (t > 0.0f)
        {
            t -= Time.deltaTime / fadeSpeed;
            c.a = t;
            image.color = c;
            yield return null;
        }
        c.a = 0.0f;
        image.color = c;

        if (OnVisible != null)
            OnVisible();
        image.gameObject.SetActive(false);

        c.a = 1.0f;
        image.color = c;
    }
    IEnumerator Swipe(Action OnHidden, Action OnVisible)
    {
        image.gameObject.SetActive(true);
        float t = 0.0f;
        float delta = 0.0f;
        Vector2 offsetMin;
        Vector2 offsetMax;
        while (t < 1.0f)
        {
            t += Mathf.Clamp01(Time.deltaTime / swipeSpeed);
            delta = (1 - t) * Screen.width + 20;
            offsetMin = image.rectTransform.offsetMin;
            offsetMin.x = -delta;
            image.rectTransform.offsetMin = offsetMin;
            offsetMax = image.rectTransform.offsetMax;
            offsetMax.x = -delta;
            image.rectTransform.offsetMax = offsetMax;
            yield return null;
        }
        delta = 0.0f;
        offsetMin = image.rectTransform.offsetMin;
        offsetMin.x = -delta;
        image.rectTransform.offsetMin = offsetMin;
        offsetMax = image.rectTransform.offsetMax;
        offsetMax.x = -delta;
        image.rectTransform.offsetMax = offsetMax;

        if (OnHidden != null)
            OnHidden();

        yield return new WaitForSeconds(0.6f);

        t = 0.0f;

        while (t < 1.0f)
        {
            t += Mathf.Clamp01(Time.deltaTime / swipeSpeed);
            delta = t * Screen.width + 20;
            offsetMin = image.rectTransform.offsetMin;
            offsetMin.x = delta;
            image.rectTransform.offsetMin = offsetMin;
            offsetMax = image.rectTransform.offsetMax;
            offsetMax.x = delta;
            image.rectTransform.offsetMax = offsetMax;
            yield return null;
        }
        delta = 1.0f;
        offsetMin = image.rectTransform.offsetMin;
        offsetMin.x = delta;
        image.rectTransform.offsetMin = offsetMin;
        offsetMax = image.rectTransform.offsetMax;
        offsetMax.x = delta;
        image.rectTransform.offsetMax = offsetMax;

        if (OnVisible != null)
            OnVisible();
        image.gameObject.SetActive(false);

        delta = 0.0f;
        offsetMin = image.rectTransform.offsetMin;
        offsetMin.x = -delta;
        image.rectTransform.offsetMin = offsetMin;
        offsetMax = image.rectTransform.offsetMax;
        offsetMax.x = -delta;
        image.rectTransform.offsetMax = offsetMax;
    }
}
