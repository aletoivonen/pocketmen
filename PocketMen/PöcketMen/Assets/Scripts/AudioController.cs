﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController Instance;

    [SerializeField] private AudioSource source;
    [SerializeField] private AudioSource musicSource;

    [SerializeField] private AudioClip[] punchSounds;

    [SerializeField] private AudioClip grassSound;
    [SerializeField] private AudioClip startSound;
    [SerializeField] private AudioClip buttonSound;
    [SerializeField] private AudioClip selectSound;
    [SerializeField] private AudioClip winSound;
    [SerializeField] private AudioClip collectSound;
    [SerializeField] private AudioClip statusEffectSound;
    [SerializeField] private AudioClip wooshSound;
    [SerializeField] private AudioClip summonSound;

    [SerializeField] private AudioClip musicStart;
    [SerializeField] private AudioClip musicGame;
    [SerializeField] private AudioClip musicBattle;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            ChooseMusic(Music.START);
        }
    }

    public void PlayRandomPunch()
    {
        source.PlayOneShot(punchSounds[Random.Range(0, punchSounds.Length)]);
    }

    public void GrassSound()
    {
        source.PlayOneShot(grassSound);
    }

    public void StartSound()
    {
        source.PlayOneShot(startSound, 0.3f);
    }

    public void ButtonSound()
    {
        source.PlayOneShot(buttonSound);
    }

    public void SelectSound()
    {
        source.PlayOneShot(selectSound);
    }

    public void WinSound()
    {
        source.PlayOneShot(winSound);
    }

    public void CollectSound()
    {
        source.PlayOneShot(collectSound, 0.3f);
    }

    public void StatusEffectSound()
    {
        source.PlayOneShot(statusEffectSound);
    }

    public void WooshSound()
    {
        source.PlayOneShot(wooshSound);
    }

    public void SummonSound()
    {
        source.PlayOneShot(summonSound);
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }

    public void ChooseMusic(Music music)
    {
        switch (music)
        {
            case Music.BATTLE:
                {
                    musicSource.clip = musicBattle;
                    musicSource.volume = 0.5f;
                    break;
                }
            case Music.START:
                {
                    musicSource.clip = musicStart;
                    musicSource.volume = 0.5f;
                    break;
                }
            case Music.GAME:
                {
                    musicSource.clip = musicGame;
                    musicSource.volume = 1.0f;
                    break;
                }
        }
        musicSource.Play();
    }

}

public enum Music
{
    START,
    GAME,
    BATTLE
}
