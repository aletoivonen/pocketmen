﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/NpcConfig", order = 4)]
public class NpcConfig : ScriptableObject
{
    public string id;
    public string visibleName;
    public Sprite sprite;
    public PocketMan[] pocketMen;
    public string greetingMessage;
}
