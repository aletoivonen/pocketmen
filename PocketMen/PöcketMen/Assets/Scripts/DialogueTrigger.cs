﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField]
    private string[] lines;
    [SerializeField]
    private Vector2 requiredMoveDir;

    public bool ShowDialogue(Action OnComplete = null)
    {
        DialogueController.Instance.ShowDialogue(lines, OnComplete);
        return true;
    }
}
