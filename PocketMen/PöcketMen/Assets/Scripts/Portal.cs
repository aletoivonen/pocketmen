﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Transform targetLocation;

    private void OnValidate()
    {
        if (targetLocation == null)
            targetLocation = transform.GetChild(0);
    }
}
