﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterTrigger : MonoBehaviour
{
    [SerializeField]
    [Tooltip("PROBABILITIES MUST ADD UP TO ONE (1)")]
    private List<SpawnProbability> areaProbabilities = new List<SpawnProbability>();

    [SerializeField]
    private float spawnChance = 0.1f;

    [SerializeField]
    private NpcConfig[] npcEncounters;

    public void CheckEncounter()
    {
        AudioController.Instance.GrassSound();

        if (Random.Range(0.0f, 1.0f) > spawnChance || !PlayerController.Instance.AnyAlive())
            return;

        if (npcEncounters != null && npcEncounters.Length > 0)
        {
            PlayerController.Instance.ToggleMovement(false);
            InvasionAlert.Instance.GetInvaded(() =>
            {
                FightController.Instance.StartFight(npcEncounters[Random.Range(0, npcEncounters.Length)]);
            });
            spawnChance = 0.0f;
            return;
        }

        float roll = Random.Range(0.0f, 1.0f);
        float rollCounter = 0.0f;
        PocketMan encounter = null;

        for (int i = 0; i < areaProbabilities.Count; i++)
        {
            SpawnProbability prob = areaProbabilities[i];
            if (roll <= prob.spawnChance + rollCounter)
            {
                encounter = prob.config;
                break;
            }
            else
            {
                rollCounter += prob.spawnChance;
            }
        }

        FightController.Instance.StartFight(new PocketManInstance(encounter));
    }
}
