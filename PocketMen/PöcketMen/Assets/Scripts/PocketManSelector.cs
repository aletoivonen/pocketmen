﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PocketManSelector : MonoBehaviour
{
    public static PocketManSelector Instance;
    public static Action<bool> OnClose;

    [SerializeField]
    private GameObject parent;
    private void Awake()
    {
        Instance = this;
    }

    [SerializeField]
    private RectTransform selectorGrid;
    [SerializeField]
    private GameObject selectorPrefab;

    [SerializeField]
    private FightStatDisplay selectedDisplay;
    [SerializeField]
    private Image selectedSprite;

    private PocketManInstance initialSelected;

    public void OpenSelector()
    {
        initialSelected = PlayerController.Instance.selectedPocketMan;
        parent.SetActive(true);
        PopulateGrid();
        Refresh();
    }
    public void CloseSelector()
    {
        if (PlayerController.Instance.selectedPocketMan.hp <= 0)
            return;
        parent.SetActive(false);
        if (OnClose != null)
            OnClose(PlayerController.Instance.selectedPocketMan != initialSelected);
    }

    private void Refresh()
    {
        selectedDisplay.Refresh(PlayerController.Instance.selectedPocketMan);
        selectedSprite.sprite = PlayerController.Instance.selectedPocketMan.config.opponentSprite;
    }

    private void PopulateGrid()
    {
        List<GameObject> destroyList = new List<GameObject>();
        foreach (Transform child in selectorGrid)
        {
            destroyList.Add(child.gameObject);
        }
        foreach (GameObject g in destroyList)
        {
            Destroy(g);
        }

        foreach (PocketManInstance instance in PlayerController.Instance.pocketManInstances)
        {
            GameObject button = Instantiate(selectorPrefab);
            button.transform.SetParent(selectorGrid, false);
            button.GetComponent<Button>().onClick.AddListener(() =>
            {
                if (instance.hp > 0)
                {
                    PlayerController.Instance.SelectPocketMan(instance);
                    Refresh();
                }
            });
            bool dead = instance.hp <= 0;
            button.GetComponentInChildren<Text>().text = (dead ? "*FAINTED*" : "") + instance.config.visibleName.ToUpper() + " LV" + instance.level;
        }
    }
}
