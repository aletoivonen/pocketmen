﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CollectableItem", order = 3)]
[System.Serializable]
public class CollectableItem : ScriptableObject
{
    public string id;
    public string visibleName;
    public Sprite sprite;
    public bool targetsPocketMan;
}