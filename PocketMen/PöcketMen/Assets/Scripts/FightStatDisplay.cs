﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightStatDisplay : MonoBehaviour
{
    [SerializeField]
    private Text nameField;
    [SerializeField]
    private Text levelField;
    [SerializeField]
    private Text hpField;
    [SerializeField]
    private Image hpBar;

    public void Refresh(PocketManInstance instance)
    {
        nameField.text = instance.config.visibleName;
        levelField.text = "LV" + instance.level.ToString();
        hpField.text = instance.hp.ToString();
        float targetFill = Mathf.Clamp01((float)instance.hp / (float)instance.config.maxHp);
        if (!enabled)
        {
            hpBar.fillAmount = targetFill;
        }
        else
        {
            StartCoroutine(RefreshHealth(targetFill));
        }
    }

    IEnumerator RefreshHealth(float targetfill)
    {
        float t = 0.0f;
        float init = hpBar.fillAmount;
        while (t < 1.0f)
        {
            t += Time.deltaTime / 0.3f;
            hpBar.fillAmount = Mathf.Clamp01(Mathf.Lerp(init, targetfill, t));
            yield return null;
        }
        hpBar.fillAmount = Mathf.Clamp01(targetfill);

    }
}
